unit uIOCPCoderDATA;

interface

uses
  SysUtils, uBufferRW;

type
  TIOCPCoderDATA = class(TObject)
  private
    FZiped:Byte;
    FNameSpaceID:Integer;
    FHeadBytes:TBytes;
    FBufferBytes:TBytes;
  public
    constructor create;
    destructor Destroy; override;
    procedure setHeadBytes(pvHead:TBytes);
    procedure setBufferBytes(pvBuffer:TBytes);

    /// <summary>
    ///   解码头部信息
    ///     建议到业务层进行解码
    /// </summary>
    procedure decodeHead();virtual;

    /// <summary>
    ///   数据
    /// </summary>
    property BufferBytes: TBytes read FBufferBytes;
    
    /// <summary>
    ///   命令nameSpaceID
    /// </summary>
    property NameSpaceID: Integer read FNameSpaceID;


    /// <summary>
    ///   0:不压缩
    ///   1:进行了压缩
    /// </summary>
    property Ziped: Byte read FZiped; 
  end;

implementation

constructor TIOCPCoderDATA.Create;
begin
  inherited Create;
end;

procedure TIOCPCoderDATA.decodeHead;
var
  lvBufferReader:IBufferReader;
begin
  FZiped:= 0;
  if Length(FHeadBytes) > 0 then
  begin
    lvBufferReader := TBufferReader.create(@FHeadBytes[0], length(FHeadBytes));
    lvBufferReader.read(FZiped, SizeOf(FZiped));
    lvBufferReader.read(FNameSpaceID, SizeOf(FNameSpaceID));
    lvBufferReader := nil;
  end;
end;

destructor TIOCPCoderDATA.Destroy;
begin
  setLength(FBufferBytes, 0);
  setLength(FHeadBytes, 0);
  inherited Destroy;
end;

procedure TIOCPCoderDATA.setBufferBytes(pvBuffer: TBytes);
begin
  FBufferBytes := pvBuffer;
end;

procedure TIOCPCoderDATA.setHeadBytes(pvHead:TBytes);
begin
  FHeadBytes := pvHead;  
end;

end.
