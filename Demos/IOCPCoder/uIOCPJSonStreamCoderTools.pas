unit uIOCPJSonStreamCoderTools;

interface

uses
  JsonStream, uIOCPCoderDATA, SysUtils, uZipTools, Classes;

type
  TIOCPJSonStreamCoderTools = class(TObject)
  public
    /// <summary>
    ///    从CoderDATA中再一次解码成JsonStream数据
    ///    0:解码成功
    /// </summary>
    class function DecodeFromCoderDATA(pvOutObject:TJSonStream; pvCoderDATA:TIOCPCoderDATA):Integer;
  end;

implementation

uses
  uJSonStreamTools;

{ TIOCPJSonStreamCoderTools }

class function TIOCPJSonStreamCoderTools.DecodeFromCoderDATA(pvOutObject: TJSonStream;
  pvCoderDATA: TIOCPCoderDATA): Integer;
var
  lvBuffer:TBytes;
  lvStream:TMemoryStream;
begin
  pvCoderDATA.decodeHead; 

  //需要进行解压
  if pvCoderDATA.Ziped = 1 then
  begin
    lvBuffer := TBytes(TZipTools.unCompressBuf(pvCoderDATA.BufferBytes[0],
      Length(pvCoderDATA.BufferBytes)));
  end else
  begin
    lvBuffer := pvCoderDATA.BufferBytes;
  end;

  lvStream:=TMemoryStream.Create;
  try
    lvStream.Write(lvBuffer[0], Length(lvBuffer));
    lvStream.Position := 0;

    //解码
    if not TJSonStreamTools.unPackFromStream(pvOutObject, lvStream) then
    begin   //解码失败
      Result := -1;
    end else
    begin
      Result := 0;
    end;
  finally
    lvStream.Free;
  end;
  
end;

end.
