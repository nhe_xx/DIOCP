unit uIOCPJsonStreamCoder;
///  版权所有<DIOCP项目-(D10.天地弦:185511468)>
///     请保留版权说明。
///  可以随意使用和复制
///
///  新的协议JSonStream打包协议
///  $0819  + HEAD_LEN + HEAD_DATA + DATA_LEN + DATA(JSON_LEN + STREAM_LEN + JSON_DATA + STREAM_DATA)
///     HEAD  zip(byte(0,1)), cmd.namespaceid(integer)
///     DATA数据> 100K进行压缩
interface

uses
  uIOCPCentre, uBuffer, Classes,
  JSonStream, uJSonStreamTools, uByteTools,
  uIOCPCoderDATA,
  uZipTools, SysUtils, uIOCPProtocol, uMyTypes;

const
  MAX_OBJECT_SIZE = 1024 * 1024 * 10;  //最大对象大小 10M , 大于10M 则会认为错误的包。

type
  TIOCPJsonStreamEncoder = class(TIOCPEncoder)
  public
    /// <summary>
    ///   编码要发生的对象
    /// </summary>
    /// <param name="pvDataObject"> 要进行编码的对象 </param>
    /// <param name="ouBuf"> 编码好的数据 </param>
    procedure Encode(pvDataObject:TObject; const ouBuf: TBufferLink); override;
  end;


  TIOCPJsonStreamDecoder = class(TIOCPDecoder)
  public
    /// <summary>
    ///   解码收到的数据,如果有接收到数据,调用该方法,进行解码
    /// </summary>
    /// <returns>
    ///   返回解码初步解码对的TIOCPCoderDATA
    /// </returns>
    /// <param name="inBuf"> 接收到的流数据 </param>
    function Decode(const inBuf: TBufferLink): TObject; override;
  end;

implementation

const
  PACK_FLAG = $0819;

  MAX_HEAD_LEN = 1024;

procedure TIOCPJsonStreamEncoder.Encode(pvDataObject:TObject; const ouBuf:
    TBufferLink);
var
  lvDataObject:TJsonStream;
  lvBytes :SysUtils.TBytes;
  lvDataLen, lvWriteL: Integer;
  lvHeadlen, lvNameSpaceID: Integer;
  lvZiped:Byte;
  lvPACK_FLAG:Word;
  lvStream:TMemoryStream;
begin
  if pvDataObject = nil then exit;
  lvDataObject := TJsonStream(pvDataObject);

  lvStream := TMemoryStream.Create;
  try
    //打包
    TJSonStreamTools.pack2Stream(lvDataObject, lvStream);
    if lvStream.Size > MAX_OBJECT_SIZE then
      raise Exception.CreateFmt('数据包太大,请在业务层分拆发送,最大数据包[%d]!', [MAX_OBJECT_SIZE]);

    lvStream.Position := 0;
    SetLength(lvBytes, lvStream.Size);
    lvStream.Read(lvBytes[0], lvStream.Size);
  finally
    lvStream.Free;
  end;

  lvDataLen :=Length(lvBytes);

  if lvDataLen > 1024 * 100 then  // >100K 进行压缩
  begin
    lvBytes := SysUtils.TBytes(TZipTools.compressBuf(lvBytes[0], lvDataLen));
    lvDataLen := Length(lvBytes);
    lvZiped := 1;
  end else
  begin
    lvZiped := 0;   //未进行压缩
  end;

  if lvDataLen > MAX_OBJECT_SIZE then
    raise Exception.CreateFmt('数据包太大,请在业务层分拆发送,最大数据包[%d]!', [MAX_OBJECT_SIZE]);

  lvNameSpaceID := lvDataObject.Json.I['cmd.namespaceid'];

  lvPACK_FLAG := PACK_FLAG;
  //pack_flag
  ouBuf.AddBuffer(@lvPACK_FLAG, 2);
  //Head_len: zip + namespaceid
  lvHeadlen := SizeOf(lvZiped) + SizeOf(lvNameSpaceID);
  lvWriteL := TByteTools.Swap32(lvHeadlen);
  //head_len
  ouBuf.AddBuffer(@lvWriteL, SizeOf(lvWriteL));

  //zip
  ouBuf.AddBuffer(@lvZiped, SizeOf(lvZiped));
  //namesapceid
  ouBuf.AddBuffer(@lvNameSpaceID, SizeOf(lvNameSpaceID));

  //data_len
  lvWriteL := TByteTools.swap32(lvDataLen);
  ouBuf.AddBuffer(@lvWriteL, SizeOf(lvWriteL));
  
  //data
  ouBuf.AddBuffer(@lvBytes[0], lvDataLen); 
end;

{ TIOCPJsonStreamDecoder }

function TIOCPJsonStreamDecoder.Decode(const inBuf: TBufferLink): TObject;
var
  lvBytes, lvHeadBytes:SysUtils.TBytes;
  lvValidCount, lvReadL:Integer;
  lvPACK_FLAG:Word;
  lvDataLen: Integer;
  lvHeadlen, lvNameSpaceID: Integer;
  lvZiped:Byte;
  lvDataObj:TIOCPCoderDATA;
begin
  Result := nil;

  //如果缓存中的数据长度不够包头长度，
  lvValidCount := inBuf.validCount;   //pack_flag + head_len + buf_len
  if (lvValidCount < SizeOf(Word) + SizeOf(Integer) + SizeOf(Integer)) then
  begin
    Exit;
  end;

  //记录读取位置
  inBuf.markReaderIndex;
  //setLength(lvBytes, 2);
  inBuf.readBuffer(@lvPACK_FLAG, 2);

  if lvPACK_FLAG <> PACK_FLAG then
  begin
    //错误的包数据
    Result := TObject(-1);
    exit;
  end;

  //headlen
  inBuf.readBuffer(@lvReadL, SizeOf(lvReadL));
  lvHeadlen := TByteTools.swap32(lvReadL);

  if lvHeadlen > 0 then
  begin
    //文件头不能过大
    if lvHeadlen > MAX_HEAD_LEN  then
    begin
      Result := TObject(-1);
      exit;
    end;

    if inBuf.validCount < lvHeadlen then
    begin
      //返回buf的读取位置
      inBuf.restoreReaderIndex;
      exit;
    end;
    
    //head
    setLength(lvHeadBytes, lvHeadlen);
    inBuf.readBuffer(@lvHeadBytes[0], lvHeadlen);
  end else if lvHeadlen < 0 then
  begin
    //错误的包数据
    Result := TObject(-1);
    exit;
  end;

  //buf_len
  inBuf.readBuffer(@lvReadL, SizeOf(lvReadL));
  lvDataLen := TByteTools.swap32(lvReadL);

  ///如果数据过大，
  if (lvDataLen > MAX_OBJECT_SIZE)  then
  begin
    //错误的包数据
    Result := TObject(-1);
    exit;
  end;
  

  //如果缓存中的数据不够json的长度和流长度<说明数据还没有收取完毕>解码失败
  lvValidCount := inBuf.validCount;
  if lvValidCount < (lvDataLen) then
  begin
    //返回buf的读取位置
    inBuf.restoreReaderIndex;
    exit;
  end;

  lvDataObj := TIOCPCoderDATA.create;
  result := lvDataObj;
  lvDataObj.setHeadBytes(lvHeadBytes);

  //读取数据长度
  if lvDataLen > 0 then
  begin
    setLength(lvBytes, lvDataLen);
    inBuf.readBuffer(@lvBytes[0], lvDataLen);
    lvDataObj.setBufferBytes(lvBytes);
  end;
end;

end.
