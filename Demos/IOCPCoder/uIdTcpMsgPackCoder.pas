unit uIdTcpMsgPackCoder;

interface

uses
  Classes, SysUtils, superobject,
  Windows,
  qmsgPack, IdTCPClient, Math, uZipTools,
  uIdTcpClientTools, uBufferRW;


const
  BUF_BLOCK_SIZE = 1024 * 10 * 50;  //50K
  
type
  TIdTcpMsgPackCoder = class(TObject)
  private
    /// <summary>
    ///   从头Buffer和数据buffer中还原TQMsgPack
    /// </summary>
    class procedure decodeFromBuffer(pvObject:TQMsgPack; pvHeadBuffer, pvBuffer:TBytes);
  public
    /// <summary>
    ///   接收解码
    /// </summary>
    /// <returns> Boolean
    /// </returns>
    /// <param name="pvSocket"> (TClientSocket) </param>
    /// <param name="pvObject"> (TObject) </param>
    class function Decode(pvSocket: TIdTcpClient; pvObject: TObject): Boolean;

    /// <summary>
    ///   编码发送
    /// </summary>
    /// <param name="pvSocket"> (TClientSocket) </param>
    /// <param name="pvDataObject"> (TObject) </param>
    class function Encode(pvSocket: TIdTcpClient; pvDataObject: TObject): Integer;

  end;

implementation

uses
  uByteTools;


const
  PACK_FLAG = $0818;  
  MAX_HEAD_LEN = 1024;

const
  MAX_OBJECT_SIZE = 1024 * 1024 * 10;  //最大对象大小 10M , 大于10M 则会认为错误的包。


class function TIdTcpMsgPackCoder.Decode(pvSocket: TIdTcpClient; pvObject:
    TObject): Boolean;
var
  lvBytes, lvHeadBytes:SysUtils.TBytes;
  lvReadL:Integer;
  lvPACK_FLAG:Word;
  lvDataLen: Integer;
  lvHeadlen: Integer;
begin
  TIdTcpClientTools.recvBuffer(pvSocket.Socket, @lvPACK_FLAG, 2);
  if lvPACK_FLAG <> PACK_FLAG then
  begin
    TIdTcpClientTools.slienceClose(pvSocket);
    //错误的包数据
    raise Exception.Create('错误的包头数据,断开与服务器的连接');
  end;

  //headlen
  TIdTcpClientTools.recvBuffer(pvSocket.Socket,@lvReadL, SizeOf(lvReadL));
  lvHeadlen := TByteTools.swap32(lvReadL);

  if lvHeadlen > 0 then
  begin
    //文件头不能过大
    if lvHeadlen > MAX_HEAD_LEN  then
    begin
      TIdTcpClientTools.slienceClose(pvSocket);
      //错误的包数据
      raise Exception.Create('错误的包头数据,断开与服务器的连接');
    end;


    //head
    setLength(lvHeadBytes, lvHeadlen);
    TIdTcpClientTools.recvBuffer(pvSocket.Socket,@lvHeadBytes[0], lvHeadlen);
  end else if lvHeadlen < 0 then
  begin
    //错误的包数据
    TIdTcpClientTools.slienceClose(pvSocket);
    //错误的包数据
    raise Exception.Create('错误的包头数据,断开与服务器的连接');
  end;

  //buf_len
  TIdTcpClientTools.recvBuffer(pvSocket.Socket,@lvReadL, SizeOf(lvReadL));
  lvDataLen := TByteTools.swap32(lvReadL);

  ///如果数据过大，
  if (lvDataLen > MAX_OBJECT_SIZE)  then
  begin
    //错误的包数据
    TIdTcpClientTools.slienceClose(pvSocket);
    //错误的包数据
    raise Exception.Create('错误的数据,断开与服务器的连接');
  end;


  //读取数据长度
  if lvDataLen > 0 then
  begin
    setLength(lvBytes, lvDataLen);
    TIdTcpClientTools.recvBuffer(pvSocket.Socket,@lvBytes[0], lvDataLen);
  end;

  decodeFromBuffer(TQMsgPack(pvObject), lvHeadBytes, lvBytes);
  Result := true;
end;

class procedure TIdTcpMsgPackCoder.decodeFromBuffer(pvObject: TQMsgPack;
  pvHeadBuffer, pvBuffer: TBytes);
var
  lvBufferReader:IBufferReader;
  lvZiped:Byte;
  lvNameSpaceID:Integer;
  lvBuffers:TBytes;
begin
  lvZiped:= 0;
  if Length(pvHeadBuffer) > 0 then
  begin
    lvBufferReader := TBufferReader.create(@pvHeadBuffer[0], length(pvHeadBuffer));
    lvBufferReader.read(lvZiped, SizeOf(lvZiped));
    lvBufferReader.read(lvNameSpaceID, SizeOf(lvNameSpaceID));
  end;

  //压缩了
  if lvZiped = 1 then
  begin
    lvBuffers := TBytes(TZipTools.unCompressBuf(pvBuffer[0], Length(pvBuffer)));
    pvObject.Parse(lvBuffers);
  end else
  begin
    //直接解析
    pvObject.Parse(pvBuffer);
  end;
end;

class function TIdTcpMsgPackCoder.Encode(pvSocket: TIdTcpClient; pvDataObject:
    TObject): Integer;
var
  lvMsgPack:TQMsgPack;
  lvBytes :SysUtils.TBytes;
  lvDataLen, lvWriteL: Integer;
  lvHeadlen, lvNameSpaceID: Integer;
  lvZiped:Byte;
  lvPACK_FLAG:Word;
  lvStream: TMemoryStream;
begin
  Result := 0;
  if pvDataObject = nil then exit;
  lvStream := TMemoryStream.create();
  try
    lvMsgPack := TQMsgPack(pvDataObject);
    lvBytes := lvMsgPack.Encode;
    lvDataLen :=Length(lvBytes);

    if lvDataLen > 1024 * 100 then  // >100K 进行压缩
    begin
      lvBytes := SysUtils.TBytes(TZipTools.compressBuf(lvBytes[0], lvDataLen));
      lvDataLen := Length(lvBytes);
      lvZiped := 1;
    end else
    begin
      lvZiped := 0;   //未进行压缩
    end;

    if lvDataLen > MAX_OBJECT_SIZE then
      raise Exception.CreateFmt('数据包太大,请在业务层分拆发送,最大数据包[%d]!', [MAX_OBJECT_SIZE]);


    if lvMsgPack.ItemByPath('cmd.namespaceid') <> nil then
    begin
      lvNameSpaceID := lvMsgPack.ForcePath('cmd.namespaceid').AsInteger;
    end else
    begin
      lvNameSpaceID := 0;
    end;

    lvPACK_FLAG := PACK_FLAG;
    //pack_flag
    lvStream.Write(lvPACK_FLAG, 2);
    //Head_len: zip + namespaceid
    lvHeadlen := SizeOf(lvZiped) + SizeOf(lvNameSpaceID);
    lvWriteL := TByteTools.swap32(lvHeadlen);
    //head_len
    lvStream.Write(lvWriteL, SizeOf(lvWriteL));

    //zip
    lvStream.Write(lvZiped, SizeOf(lvZiped));
    //namesapceid
    lvStream.Write(lvNameSpaceID, SizeOf(lvNameSpaceID));

    //data_len
    lvWriteL := TByteTools.swap32(lvDataLen);
    lvStream.Write(lvWriteL, SizeOf(lvWriteL));
    //data
    lvStream.Write(lvBytes[0], lvDataLen);

    lvStream.Position := 0;
    Result := TIdTcpClientTools.sendStream(pvSocket.Socket,lvStream);
  finally
    lvStream.Free;
  end;
end;

end.
