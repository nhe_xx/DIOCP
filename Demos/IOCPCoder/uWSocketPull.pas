unit uWSocketPull;

interface

uses
  Classes, OverbyteIcsWSocket, SysUtils, Windows,
  Messages, qmsgpack, uWSocketMsgPackCoder, ComObj,
  FileLogger,
  SyncObjs;

type
  TDisplayEvent = procedure(Sender : TObject; const Msg : String) of object;

  TWSocketPull = class;
  TMonitor = class;

  /// <summary>
  ///   WSocket线程处理类
  ///     建立线程内部的WSocket
  /// </summary>
  TWSocketThread = class(TThread)
  private
    FTempBytes:array[0..1] of Byte;

    FMsgINfo:String;
    FOwner: TWSocketPull;
    FSocket: TWSocket;
    procedure Display(const Msg: String);
    procedure OnSessionConnected(Sender: TObject; ErrCode: WORD);
    procedure OnDataAvailable(Sender: TObject; ErrCode: Word);
    procedure OnSessionClosed(Sender: TObject; ErrCode: Word);
    procedure onSockError(Sender : TObject; Error : Integer; Msg : String);
    
    /// <summary>
    ///   获取一个对象
    /// </summary>
    procedure recvObject;

    /// <summary>
    ///   检测连接是否已经连接, 如果已经连接返回true，
    ///     如果没有连接，则进行发出命令建立连接
    /// </summary>
    function checkConnect: Boolean;
    
  public
    constructor Create(AOwner: TWSocketPull);

    /// <summary>
    ///   处理消息
    /// </summary>
    procedure Execute; override;

    /// <summary>
    ///   发送一个对象
    /// </summary>
    procedure sendObject(pvMsgPack:TQMsgPack);

    /// <summary>
    ///   停止线程
    /// </summary>
    procedure Terminate;
  end;

  /// <summary>
  ///   接受消息推送
  /// </summary>
  TWSocketPull = class(TObject)
  private
    /// <summary>
    ///   Socket 如果准备好会有信号
    /// </summary>
    FSocketReady: TEvent;
    
    FIntervalOfCheckOnline: Integer;
    FMsgINfo:String;

    FHost:String;
    FPort:String;
    FIcsThread:TWSocketThread;
    FIsSendHeartPackage: Boolean;
    FMonitor:TMonitor;
    
    FOnDisplay: TDisplayEvent;
    FOnRecvObject: TNotifyEvent;
    FPullID: String;

    procedure Display(const Msg: String);

    /// <summary>
    ///   向服务端注册接收器
    /// </summary>
    procedure register2Pull;

    procedure RecvMsgPack(pvMsgPack:TQMsgPack);
  public
    constructor Create;
    destructor Destroy; override;
    procedure setHost(pvHost:String);
    procedure setPort(pvPort:String);

    procedure start();
    procedure stop();

    /// <summary>
    ///   心跳发送时间
    /// </summary>
    property IntervalOfCheckOnline: Integer read FIntervalOfCheckOnline write
        FIntervalOfCheckOnline;



    /// <summary>
    ///   收到数据包后触发事件,
    ///     事件函数里面需要处理Sener:TObject的释放
    /// </summary>
    property OnRecvObject: TNotifyEvent read FOnRecvObject write FOnRecvObject;

    /// <summary>
    ///   是否在检测在线状态时发送心跳包
    /// </summary>
    property IsSendHeartPackage: Boolean read FIsSendHeartPackage write
        FIsSendHeartPackage;
        
    /// <summary>
    ///   通过该ID向服务端注册，服务端有该ID的消息，则进行分发。
    /// </summary>
    property PullID: String read FPullID write FPullID;

    /// <summary>
    ///   一些消息的显示, 可能有线程内部触发。
    /// </summary>
    property OnDisplay: TDisplayEvent read FOnDisplay write FOnDisplay;
  end;

  /// <summary>
  ///   主要做心跳包的发送线程
  /// </summary>
  TMonitor = class(TThread)
  private
    FHeartObject: TQMsgPack;
    FEvent: TEvent;
    FBreak:Boolean;
    FOwner: TWSocketPull;

    /// <summary>
    ///   发送一个线程
    /// </summary>
    procedure executeCheckOnline;
  public
    constructor Create(AOwner: TWSocketPull);
    destructor Destroy; override;
    procedure Execute; override;
    procedure Terminate;
  end;


implementation

function TWSocketThread.checkConnect: Boolean;
begin
  Result := false;
  if FSocket = nil then Exit;
  
  if (FSocket.State in [wsConnected, wsConnecting]) then
  begin

  end else
  begin
    FSocket.Port  := FOwner.FPort;
    FSocket.Addr  := FOwner.FHost;
    FSocket.Connect;
  end;

  Result := FSocket.State = wsConnected; 
end;

constructor TWSocketThread.Create(AOwner: TWSocketPull);
begin
  inherited Create(True);
  FOwner := AOwner;
  FSocket := nil;
end;

procedure TWSocketThread.Display(const Msg : String);
begin
  if Assigned(FOwner) then FOwner.Display(Msg);
end;

{ TWSocketThread }

procedure TWSocketThread.Execute;
begin
    Display('Thread start');
    FSocket := TWSocket.Create(nil);
    try
        FSocket.Name  := 'WSocket1';
        FSocket.Proto := 'tcp';
        FSocket.ComponentOptions := FSocket.ComponentOptions + [wsoNoReceiveLoop];
        FSocket.OnSessionConnected := OnSessionConnected;
        FSocket.OnSessionClosed := OnSessionClosed;
        FSocket.OnDataAvailable := OnDataAvailable;
        FSocket.OnSocksError := onSockError;


        try
          /// 建立连接，屏蔽错误消息
          checkConnect;
        except
        end;

        FOwner.FSocketReady.SetEvent;
        FSocket.MessageLoop;
    finally
        FSocket.Free;
    end;
    TFileLogger.instance.logMessage('接受消息线程已经停止!', 'ALIVE_DEBUG_');
    Display('Thread stopped');
end;

procedure TWSocketThread.sendObject(pvMsgPack: TQMsgPack);
begin
  //编码 注册
  TWSocketMsgPackCoder.Encode(FSocket, pvMsgPack);
end;

procedure TWSocketThread.OnDataAvailable(Sender: TObject; ErrCode: Word);
begin
  if ErrCode = 0 then
  begin
    recvObject();    
  end else
  begin
    FMsgINfo := 'OnDataAvailable, errorCode:' +  IntToStr(ErrCode);
    TFileLogger.instance.logMessage(FMsgINfo, 'MessagePull_');
    Display(FMsgINfo);
  end;
end;

procedure TWSocketThread.OnSessionClosed(Sender: TObject; ErrCode: Word);
begin
  FMsgINfo := 'OnSessionClosed, errorCode:' +  IntToStr(ErrCode);
  TFileLogger.instance.logMessage(FMsgINfo, 'ALIVE_DEBUG_');
  Display(FMsgINfo);
end;

procedure TWSocketThread.onSockError(Sender: TObject; Error: Integer; Msg: String);
begin
  FMsgINfo := Format('onSockError:%d, %s', [Error, Msg]);
  TFileLogger.instance.logMessage(FMsgINfo, 'MessagePull_');
  Display(FMsgINfo);
end;

procedure TWSocketThread.OnSessionConnected(Sender: TObject; ErrCode: WORD);
begin
  if ErrCode <> 0 then
  begin
    FMsgINfo :=TWSocket(Sender).Name + ': 不能建立连接. 错误代码 #' + IntToStr(ErrCode);
    TFileLogger.instance.logMessage(FMsgINfo, 'ALIVE_DEBUG_');
    Display(FMsgINfo);
  end else
  begin
    FMsgINfo := TWSocket(Sender).Name + ': 已经连接!';
    TFileLogger.instance.logMessage(FMsgINfo, 'ALIVE_DEBUG_');
    Display(FMsgINfo); 
    FOwner.register2Pull;
  end;
end;

procedure TWSocketThread.Terminate;
begin
  PostMessage(FSocket.Handle, WM_QUIT, 0, 0);
  TFileLogger.instance.logMessage('主动调用Terminate发送WM_QUIT!', 'ALIVE_DEBUG_');
  inherited Terminate;
end;

procedure TWSocketThread.recvObject;
var
  lvMsgPack:TQMsgPack;
begin
  // 等待是否有数据接收
  if FSocket.RcvdCount = 0 then
  begin
    //进行一次接受，要不然进行阻塞。
    FSocket.Receive(@FTempBytes[0], 0);
    TFileLogger.instance.logMessage('接收到0的数据包', 'DEBUG_');
  end else if FSocket.RcvdCount > 0 then
  begin
    lvMsgPack := TQMsgPack.Create;
    try
      TFileLogger.instance.logMessage('准备接收到一个数据包', 'DEBUG_SPEED_');
      TWSocketMsgPackCoder.Decode(FSocket, lvMsgPack);
      TFileLogger.instance.logMessage('接收到一个数据包完成', 'DEBUG_SPEED_');
    except
      on E:Exception do
      begin
        FMsgINfo := '解码异常('+e.ClassName+'):' + e.Message;
        try
          lvMsgPack.Free;
          lvMsgPack := nil;
        except
        end;
        TFileLogger.instance.logMessage(FMsgINfo, 'MessagePull_');
      end;
    end;
    if lvMsgPack <> nil then
    begin
      ///  投递到 Pull 进行处理
      if Assigned(FOwner) then
      begin
        FOwner.RecvMsgPack(lvMsgPack);
      end else
      begin
        lvMsgPack.Free;
      end;
    end;
  end;
end;

constructor TWSocketPull.Create;
begin
  inherited Create;
  FIsSendHeartPackage := true;
  FSocketReady := TEvent.Create(nil, True, False, '');
  FPullID := CreateClassID;
  FIntervalOfCheckOnline := 20000;  // 10 秒一次检测
end;

destructor TWSocketPull.Destroy;
begin
  FSocketReady.Free;
  inherited Destroy;
end;

procedure TWSocketPull.Display(const Msg : String);
begin
  if Assigned(OnDisplay) then OnDisplay(Self, Msg);
end;

procedure TWSocketPull.RecvMsgPack(pvMsgPack: TQMsgPack);
begin
  if Assigned(FOnRecvObject) then
  begin        /// 如果外包 外部有事件，投递到外部进行处理
    FOnRecvObject(pvMsgPack);
  end else
  begin
    try
      FIcsThread.sendObject(pvMsgPack);
      Display(pvMsgPack.AsString);
    finally
      pvMsgPack.Free;
    end;
  end;
end;

procedure TWSocketPull.register2Pull;
var
  lvMsgPack:TQMsgPack;
begin
  try
    lvMsgPack := TQMsgPack.Create;
    try
      //注册Pull
      lvMsgPack.ForcePath('cmd.namespaceid').AsInteger := 8810;
      lvMsgPack.ForcePath('cmd.index').AsInteger := 1;
      lvMsgPack.ForcePath('cmd.pullid').AsString := FPullID;
      FIcsThread.sendObject(lvMsgPack);
    finally
      lvMsgPack.Free;
    end;
    FMsgINfo := '成功注册Pull';
  except
    on E:Exception do
    begin
      FMsgINfo := '注册Pull异常:' + e.Message;
      TFileLogger.instance.logMessage(FMsgINfo, 'MessagePull_');
    end;
  end;
end;

procedure TWSocketPull.setHost(pvHost: String);
begin
  FHost := pvHost;
end;

procedure TWSocketPull.setPort(pvPort: String);
begin
  FPort := pvPort;
end;

procedure TWSocketPull.start;
begin
  //Socket 尚未准备好
  FSocketReady.ResetEvent;

  FIcsThread := TWSocketThread.Create(Self);
  FIcsThread.FreeOnTerminate := true;
  FIcsThread.Resume;
  FMonitor := TMonitor.Create(Self);
  FMonitor.FreeOnTerminate := true;
  FMonitor.Resume;
end;

procedure TWSocketPull.stop;
begin
  //让等待继续,这样可以让线程退出等待
  FSocketReady.SetEvent;
    
  if FMonitor <> nil then
  begin
    FMonitor.FOwner := nil;
    FMonitor.Terminate;
    FMonitor := nil;
  end;

  if FIcsThread <> nil then
  begin
    FIcsThread.FOwner := nil;
    FIcsThread.Terminate;
    FIcsThread := nil;
  end; 
end;

{ TMonitor }

constructor TMonitor.Create(AOwner: TWSocketPull);
begin
  inherited Create(True);
  FOwner := AOwner;
  FEvent := TEvent.Create(nil, True, False, '');

  // 心跳包
  FHeartObject := TQMsgPack.Create;
  FHeartObject.ForcePath('cmd.index').AsInteger := 1;  //CMD_HEARTBEAT;  
end;

destructor TMonitor.Destroy;
begin
  FEvent.Free;
  FHeartObject.Free;
  inherited Destroy;
end;

procedure TMonitor.Execute;
begin
  while (not self.Terminated) and Assigned(FOwner) do
  begin
    if FOwner.FSocketReady.WaitFor(1000) = wrSignaled then
    begin
      executeCheckOnline;
      if FOwner = nil then Break;
      FEvent.WaitFor(FOwner.FIntervalOfCheckOnline);
    end;
  end;
end;

procedure TMonitor.executeCheckOnline;
var
  lvMsg:String;
begin
  try
    if FOwner = nil then exit;
    if FOwner.FIcsThread.checkConnect then
    begin
      if FOwner = nil then exit;
      if FOwner.IsSendHeartPackage then
      begin
        FOwner.FIcsThread.sendObject(FHeartObject);
        TFileLogger.instance.logMessage('发送了一个心跳包', 'HEART_DEBUG_');
      end;
    end;
  except
    on E:Exception do
    begin
      if FOwner <> nil then
      begin
        lvMsg :=Format('心跳出现异常[%s:%s]:%s', [FOwner.FHost, FOwner.FPort, e.Message]);
      end else
      begin
        lvMsg :=Format('心跳出现异常:%s', [e.Message]);
      end;
      if FOwner <> nil then FOwner.Display(lvMsg);
      TFileLogger.instance.logMessage(lvMsg, 'MessagePull_');

//      try
//         if FOwner <> nil then
//         begin
//           FOwner.FIcsThread.checkConnect;
//         end;
//      except
//      end;
    end;                                                                                    
  end;
end;

procedure TMonitor.Terminate;
begin
  inherited Terminate;
  FEvent.SetEvent;
end;

end.
